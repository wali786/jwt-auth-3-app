export default class User {
  constructor(token, name, email, password) {
    this.token = token;
    this.name = name;
    this.email = email;
    this.password = password;
  }
}
