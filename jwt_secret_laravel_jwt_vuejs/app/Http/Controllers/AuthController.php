<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class AuthController extends Controller
{
    /**
     * Register a new user
     */
    public function register(Request $request)
    {
       if("UZm5uF0fjUFwHgemi9JpQeWsDKFAFTikD2MR013u"==$request->token){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->json(['status' => 'success', 'user'=>$user], 200);
       }else{
        return response()->json(['status' => 'Token Fail'], 200);
       }
        // $v = $request->validate([
        //     'name' => 'required|min:3',
        //     'email' => 'required|email|unique:users',
        //     'password'  => 'required|min:3|confirmed',
        // ]);
      
        // if ($v)
        // {
        //     return response()->json([
        //         'status' => 'error',
        //         'errors' => 'erorr',
        //     ], 422);
        // }
       
    }
    /**
     * Login user and return a token
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            $user= Auth::user();
            return response()->json(['status' => 'success', 'user'=>$user], 200)->header('Authorization', $token);
        }
        return response()->json(['error' => 'login_error'], 401);
    }
    /**
     * Logout User
     */

    public function userGet(){
        $users= User::user();
        return response()->json($users);
    }

    public function logout()
    {
        $this->guard()->logout();
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }
    /**
     * Get authenticated user
     */
    public function user(Request $request)
    {
       // $user = User::find(Auth::user()->id);
       $user= Auth::user();
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            return response()
                ->json(['status' => 'successs'], 200)
                ->header('Authorization', $token);
        }
        return response()->json(['error' => 'refresh_token_error'], 401);
    }
    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }
}